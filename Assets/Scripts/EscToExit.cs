﻿using UnityEngine;
using System.Collections;

public class EscToExit : MonoBehaviour 
{
    void Awake()
    {
        ResourceManager.Initialize();
        AudioSource ass;
        if (Application.loadedLevelName != "bosslevel")
        {
            ass = Camera.main.gameObject.PlaySound(ResourceManager.BackgroundAudio1, GameObjectExtensions.SoundOverrideType.SimultaneousPlaying);
        }
        else
        {
            ass = Camera.main.gameObject.PlaySound(ResourceManager.BackgroundAudio2, GameObjectExtensions.SoundOverrideType.SimultaneousPlaying);
        }
        ass.loop = true;
        ass.volume = 0.5f;
    }

    void Update () 
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            print("Exit!");
            Application.Quit();
        }
        if (Input.GetKey(KeyCode.R))
        {
            Application.LoadLevel(Application.loadedLevelName);
        }
	}
}
