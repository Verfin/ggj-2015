﻿using UnityEngine;
using System.Collections.Generic;

public class BombDeathRadius : MonoBehaviour
{
    public List<GameObject> destroyTheseGameObjects;

	// Use this for initialization
	void Start () {
        destroyTheseGameObjects = new List<GameObject>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject != null)
        {
            if (col.gameObject.layer == 8 && col.gameObject.tag != "Indestructible" || col.gameObject.layer == 11 && col.gameObject.tag != "Indestructible")
            {
                destroyTheseGameObjects.Add(col.gameObject);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject != null)
        {
            if (col.gameObject.layer == 8)
            {
                destroyTheseGameObjects.Remove(col.gameObject);
            }
        }
    }
}
