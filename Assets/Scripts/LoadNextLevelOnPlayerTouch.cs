﻿using UnityEngine;
using System.Collections;

public class LoadNextLevelOnPlayerTouch : MonoBehaviour 
{
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Player>() != null)
        {
            Application.LoadLevel(Application.loadedLevel + 1);
        }
    }
}
