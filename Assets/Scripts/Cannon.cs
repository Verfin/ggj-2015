﻿using UnityEngine;
using System.Collections;

public class Cannon : MonoBehaviour 
{
    public GameObject BallPrefab;
    public float ShootFrequency = 2f;
    public float ShootPower = 6f;
    public Vector2 BulletOffset;
    public float AngleOffset;
    public GameObject ExplosionAnimator;
    public float TriggerRange;
    public bool RandomShootDelay;
    private float shootTimer;
    private Player player;
    
	// Use this for initialization
	void Start () 
    {
        player = FindObjectOfType<Player>();
        shootTimer = Random.Range(0, 2);
	}
	
	// Update is called once per frame
	void Update () 
    {
        shootTimer -= Time.deltaTime;

        if (player != null)
        {
            if (shootTimer < 0 && (player.transform.position - gameObject.transform.position).magnitude < TriggerRange)
            {
                Shoot();
                if (RandomShootDelay)
                {
                    shootTimer = ShootFrequency + Random.Range(-0.2f, 0.2f);
                }
                else
                {
                    shootTimer = ShootFrequency;
                }
            }
        }
	}

    private void Shoot()
    {
        gameObject.PlaySound(ResourceManager.Laserpiu, GameObjectExtensions.SoundOverrideType.SimultaneousPlaying).volume = 0.07f;
        ((GameObject)GameObject.Instantiate(BallPrefab, new Vector3(transform.position.x, transform.position.y, -0.02f) + (Vector3)BulletOffset, Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, 0, AngleOffset)))).rigidbody2D.velocity = ((Vector2)transform.right * ShootPower).Rotate(AngleOffset);
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position + (Vector3)BulletOffset, ((Vector2)transform.right).Rotate(AngleOffset));
        Gizmos.DrawWireSphere(gameObject.transform.position, TriggerRange);
    }
}
