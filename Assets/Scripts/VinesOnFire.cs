﻿using UnityEngine;
using System.Collections;

public class VinesOnFire : MonoBehaviour 
{
    public bool OnFire { get; private set; }
    public float Durability = 100f;
    Animator[] anim;
    private Light ValonLapsi;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (!OnFire)
        {
            WoodenBranch wb = col.GetComponent<WoodenBranch>();
            if (wb != null)
            {
                if (wb.OnFire)
                {
                    OnFire = true;
                    foreach (Animator a in anim)
                    {
                        a.SetTrigger("Fire");
                    }
                    AudioSource ass = gameObject.PlaySound(ResourceManager.Burning, GameObjectExtensions.SoundOverrideType.SimultaneousPlaying);
                    ass.loop = true;
                    ass.volume = 0.15f;
                }
            }
        }
    }

	// Update is called once per frame
    void Update()
    {
        if (OnFire)
        {
            foreach (Light valo in GetComponentsInChildren<Light>())
            {
                valo.enabled = true;
            }
            Durability -= Time.deltaTime * 30f;
            if (Durability <= 0)
            {
                gameObject.Destroy();
            }
        }
    }

    void Start()
    {
        ValonLapsi = GetComponentInChildren<Light>();
        anim = GetComponentsInChildren<Animator>();
        foreach (Light valo in GetComponentsInChildren<Light>())
        {
            valo.enabled = false;
        }
    }
}
