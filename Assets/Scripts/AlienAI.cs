﻿using UnityEngine;
using System.Collections;

public class AlienAI : MonoBehaviour
{
    private Vector3 velocity;
    private float stuckTimer;
    private Vector3 tempLocation;
    private float yBoost;
    private float xAdjust;
    public float MaximumSpeed;
    public int FacingRight = 1;
    public RaycastHit2D Ray;
    public float RayOffset;
    public float RayLength;
    private Player player;
    private float timer;
    private PortableBlackHoleController blackhole;
    private Animator anim;
    private bool jump;

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<Player>();
        velocity = Vector3.zero;
        stuckTimer = 1;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player != null)
        {
            if (Mathf.Abs(rigidbody2D.velocity.x) < MaximumSpeed)
            {
                rigidbody2D.AddForce(transform.right * MaximumSpeed * FacingRight);
            }

            if (jump)
            {
                rigidbody2D.AddForce(new Vector2(0, 400f));
                jump = false;
            }
        }
    }

    void Flip()
    {
        FacingRight = -FacingRight;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position + transform.right * RayOffset * FacingRight, transform.right * RayLength);
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            if (player.transform.position.x < transform.position.x && FacingRight == 1)
            {
                Flip();
            }
            else if (player.transform.position.x > transform.position.x && FacingRight == -1)
            {
                Flip();
            }

            stuckTimer -= Time.deltaTime;

            if (stuckTimer < 0)
            {
                if (Mathf.Abs(Mathf.Abs(tempLocation.x) - Mathf.Abs(transform.position.x)) < 0.1f)
                {
                    jump = true;
                    anim.SetTrigger("Jump");
                }
                tempLocation = transform.position;
                stuckTimer = 1f;
            }
        }
    }
}
