﻿using UnityEngine;
using System.Collections;

public static class ResourceManager
{
    public static AudioClip BackgroundAudio1;
    public static AudioClip BackgroundAudio2;
    public static AudioClip Burning;
    public static AudioClip BossBurning;
    public static AudioClip Buttom;
    public static AudioClip Dead;
    public static AudioClip Explosion;
    public static AudioClip Laserpiu;
    public static AudioClip Ovi;
    public static AudioClip BossYell;
    public static AudioClip Poks;
    public static AudioClip Tykki;
    private static bool initialized = false;

    public static string Initialize()
    {
        if (!initialized)
        {
            try
            {
                BackgroundAudio1 = Resources.Load<AudioClip>("back1");
                BackgroundAudio2 = Resources.Load<AudioClip>("back2");
                Burning = Resources.Load<AudioClip>("Burning");
                Buttom = Resources.Load<AudioClip>("Buttom1");
                Dead = Resources.Load<AudioClip>("dead");
                Explosion = Resources.Load<AudioClip>("Explosion2");
                Laserpiu = Resources.Load<AudioClip>("laserpiu");
                Ovi = Resources.Load<AudioClip>("Ovi");
                Poks = Resources.Load<AudioClip>("poks");
                Tykki = Resources.Load<AudioClip>("Tykki3");
                BossYell = Resources.Load<AudioClip>("boss1");
                BossBurning = Resources.Load<AudioClip>("boss2");
            }
            catch (System.Exception ex)
            {
                Debug.Log(ex.Message);
                return ex.Message;
            }

            initialized = true;
        }

        return "No error loading";
    }

}
