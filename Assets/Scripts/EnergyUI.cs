﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnergyUI : MonoBehaviour 
{
    PortableBlackHoleController cube;
    Slider slider;
	// Use this for initialization
	void Awake ()
    {
        cube = FindObjectOfType<PortableBlackHoleController>();
        slider = GetComponent<Slider>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        slider.value = cube.Energy;
	}
}
