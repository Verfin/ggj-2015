﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    public GameObject Player;
    private float StartZ;

    void Start()
    {
        StartZ = transform.position.z;
        if (Player == null)
        {
            try
            {
                Player = FindObjectOfType<Player>().gameObject;
            }
            catch
            {
                print("lisää pelaaja");
            }
        }
    }

	// Update is called once per frame
    void Update()
    {
        if (Player != null)
        {
            transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, StartZ);
        }
    }
}
