﻿using UnityEngine;
using System.Collections;

public class WoodenBranch : MonoBehaviour 
{
    public bool OnFire { get; private set; }
    public float Durability = 100f;
    Animator anim;
    private Light ValonLapsi;

	// Use this for initialization
	void Start () 
    {
        ValonLapsi = GetComponentInChildren<Light>();
        anim = GetComponent<Animator>();
        ValonLapsi.enabled = false;
    }

    public void Fire()
    {
        OnFire = true;
        AudioSource ass = gameObject.PlaySound(ResourceManager.Burning, GameObjectExtensions.SoundOverrideType.SimultaneousPlaying);
        ass.loop = true;
        ass.volume = 0.15f;
        anim.SetTrigger("Fire");
    }

	// Update is called once per frame
	void Update () 
    {
        if (OnFire)
        {
            ValonLapsi.enabled = true;
            Durability -= Time.deltaTime * 20f;
            if (Durability <= 0)
            {
                gameObject.Destroy();
            }
        }
	}
}
