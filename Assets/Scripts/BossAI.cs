﻿using UnityEngine;
using System.Collections.Generic;

public class BossAI : MonoBehaviour
{
    private enum MoveState
    {
        Idle,
        Monsterdrop,
        Immafiringmahlazor,
        Gravitywell,
        GravityHell,
        RebelYell,
        Cannonballz,
        BurningBranch,
        TheSkyIsFalling
    }

    public int HP;
    public List<GravityField> GravityfieldList;
    public List<GravityField> OtherGravityFieldList;
    public List<GameObject> MonsterSpawnPointList;
    public GameObject MonsterPrefab;
    public GameObject Lazor;
    public GameObject BranchSpawnPoint;
    public GameObject BranchPrefab;
    public Vector2 LazorOffset;
    public float AngleOffset;
    public GameObject CannonBall;
    public GameObject CannonBallSpawnPoint;
    public GameObject SkyLazerSpawnPoint;
    float timer;
    MoveState state;
    MoveState lastState;
    public bool OnFire { get; private set; }
    public float Durability = 100f;
    private Light ValonLapsi;
    private float firetimer;
    private bool loadCredits;
    private Player player;
    private Animator anim;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (!OnFire)
        {
            WoodenBranch wb = col.GetComponent<WoodenBranch>();
            if (wb != null)
            {
                if (wb.OnFire)
                {
                    HP--;
                    if (HP <= 0)
                    {
                        renderer.enabled = false;
                        loadCredits = true;
                    }
                    OnFire = true;
                    anim.SetBool("Fire", true);
                    transform.GetChild(0).renderer.enabled = true;
                    AudioSource ass = gameObject.PlaySound(ResourceManager.BossBurning, GameObjectExtensions.SoundOverrideType.SimultaneousPlaying);
                    ass.volume = 0.5f;
                    firetimer = 4f;
                    wb.gameObject.Destroy();
                }
            }
        }
    }


    // Use this for initialization
    void Start()
    {
        state = MoveState.Idle;
        HP = 3;
        timer = 5f;
        OnFire = false;
        player = FindObjectOfType<Player>();
        anim = transform.GetChild(0).GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            if (OnFire)
            {
                firetimer -= Time.deltaTime;
                if (firetimer <= 0)
                {
                    if (loadCredits)
                    {
                        Application.LoadLevel(Application.loadedLevel + 1);
                    }
                    transform.GetChild(0).renderer.enabled = false;
                    OnFire = false;
                    anim.SetBool("Fire", false);
                }
            }

            if (!loadCredits)
            {
                timer -= Time.deltaTime;
                if (timer <= 0)
                {
                    timer = Random.Range(5f, 8f);
                    while (state == lastState)
                    {
                        state = (MoveState)Random.Range(0, 9);
                    }

                    switch (state)
                    {
                        case MoveState.Gravitywell:
                            foreach (GravityField grav in GravityfieldList)
                            {
                                grav.renderer.enabled = true;
                                grav.collider2D.enabled = true;
                                grav.enabled = true;
                            }

                            break;
                        case MoveState.Idle:
                            break;
                        case MoveState.Immafiringmahlazor:
                            gameObject.PlaySound(ResourceManager.Laserpiu, GameObjectExtensions.SoundOverrideType.SimultaneousPlaying).volume = 0.2f;
                            for (int i = 0; i < 12; i++)
                            {
                                ((GameObject)GameObject.Instantiate(Lazor, new Vector3(transform.position.x, transform.position.y, -0.02f) + (Vector3)LazorOffset, Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, 0, i * 9.3f + AngleOffset)))).rigidbody2D.velocity = ((Vector2)transform.right * 6).Rotate(i * 9.3f + AngleOffset);
                            }

                            break;
                        case MoveState.Monsterdrop:
                            foreach (GameObject go in MonsterSpawnPointList)
                            {
                                Instantiate(MonsterPrefab, go.transform.position, go.transform.rotation);
                            }
                            break;
                        case MoveState.RebelYell:
                            gameObject.PlaySound(ResourceManager.BossYell, GameObjectExtensions.SoundOverrideType.SimultaneousPlaying);
                            break;
                        case MoveState.BurningBranch:
                            Instantiate(BranchPrefab, BranchSpawnPoint.transform.position, BranchSpawnPoint.transform.rotation);
                            break;
                        case MoveState.Cannonballz:
                            Instantiate(CannonBall, CannonBallSpawnPoint.transform.position, CannonBallSpawnPoint.transform.rotation);
                            break;
                        case MoveState.GravityHell:
                            foreach (GravityField grav in OtherGravityFieldList)
                            {
                                grav.renderer.enabled = true;
                                grav.collider2D.enabled = true;
                                grav.enabled = true;
                            }
                            break;
                        case MoveState.TheSkyIsFalling:
                            for (int i = 0; i < 30; i++)
                            {
                                Instantiate(Lazor, SkyLazerSpawnPoint.transform.position + Vector3.right * 1.5f * i, SkyLazerSpawnPoint.transform.rotation);
                            }
                            break;
                    }

                    switch (lastState)
                    {
                        case MoveState.Gravitywell:
                            foreach (GravityField grav in GravityfieldList)
                            {
                                grav.renderer.enabled = false;
                                grav.enabled = false;
                                grav.collider2D.enabled = false;
                                player.rigidbody2D.gravityScale = 1f;
                            }

                            break;
                        case MoveState.GravityHell:
                            foreach (GravityField grav in OtherGravityFieldList)
                            {
                                grav.renderer.enabled = false;
                                grav.collider2D.enabled = false;
                                grav.Disable();
                                grav.enabled = false;
                                player.rigidbody2D.gravityScale = 1f;
                            }

                            break;
                        default:
                            break;
                    }

                    lastState = state;
                }
            }
        }
    }
}