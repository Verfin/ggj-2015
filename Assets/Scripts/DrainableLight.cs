﻿using UnityEngine;
using System.Collections;

public class DrainableLight : MonoBehaviour
{
    public float Amount = 34f;
    public Sprite ChangeSpriteToThisAfterDraining;
    private float StartAmount;
    private float LightRange;
    private float RemovableLightAmount;
    private float LightPercent;
    private Light lighter;

    public void Start()
    {
        lighter = GetComponentInChildren<Light>();
        RemovableLightAmount = lighter.range - 1;
        StartAmount = Amount;
    }

    public void Update()
    {
        LightPercent = Amount / StartAmount;
        lighter.range = RemovableLightAmount * LightPercent + 1;
    }
}
