﻿using UnityEngine;
using System.Collections;

public class LoadLevelAfter : MonoBehaviour 
{
    public float WaitTime = 3f;
    public bool SkipWithKeys = true;

	// Update is called once per frame
    void Update()
    {
        WaitTime -= Time.deltaTime;
        if (WaitTime <= 0)
        {
            Application.LoadLevel(Application.loadedLevel + 1);
        }

        if (SkipWithKeys)
        {
            if (Input.anyKey)
            {
                Application.LoadLevel(Application.loadedLevel + 1);
            }
        }
    }
}
