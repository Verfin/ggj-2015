﻿using UnityEngine;
using System.Collections;

public class DrainableMatter : MonoBehaviour {
    public float Amount = 50;
    public float StartAmount;
    private Vector3 StartSize;
    private float SizePercent;
    private float RemovableAmount = 90;
	// Use this for initialization

    public void Start()
    {
        StartSize = transform.localScale;
        StartAmount = Amount;
    }

    public void Update()
    {
        SizePercent = Amount / StartAmount;
        transform.localScale = StartSize * SizePercent;
    }
}
