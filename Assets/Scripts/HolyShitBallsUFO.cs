﻿using UnityEngine;
using System.Collections;

public class HolyShitBallsUFO : MonoBehaviour
{
    public float MaximumSpeed;
    public float Acceleration;
    public float TurningSpeed;
    public float TriggerRange;
    private Player player;
    private PortableBlackHoleController blackhole;
    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<Player>();
        blackhole = FindObjectOfType<PortableBlackHoleController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player != null)
        {
            if ((player.transform.position - gameObject.transform.position).magnitude < TriggerRange)
            {
                //rigidbody2D.velocity = transform.right * MaximumSpeed;
                if (Mathf.Abs(rigidbody2D.velocity.x) < MaximumSpeed)
                {
                    rigidbody2D.AddForce(transform.right * Acceleration);
                }
            }
        }
    }

    void Update()
    {
        if (player != null)
        {
            if ((player.transform.position - gameObject.transform.position).magnitude < TriggerRange)
            {
                Vector3 vectorToTarget = player.transform.position - transform.position;
                float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
                Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = q;
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(gameObject.transform.position, TriggerRange);
    }
}
