﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Creditroll : MonoBehaviour 
{
    public float ScrollSpeed = 5f;
    Text t;
    float timer = 29f;

    void Start()
    {
        t = GetComponent<Text>();
    }

	// Update is called once per frame
	void Update () 
    {
        t.rectTransform.position = new Vector3(t.rectTransform.position.x, t.rectTransform.position.y + Time.deltaTime * ScrollSpeed, 0);
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Application.Quit();
            Debug.Log("quit");
        }
    }
}
