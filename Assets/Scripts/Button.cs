﻿using UnityEngine;
using System.Collections.Generic;

public class Button : MonoBehaviour 
{
    public List<Door> ConnectedDoors;
    public Sprite ButtonOffSprite;
    SpriteRenderer rend;

    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        foreach (Door door in ConnectedDoors)
        {
            door.ButtonHookCount++;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Open();
    }

    public void Open()
    {
        foreach (Door door in ConnectedDoors)
        {
            door.ButtonPushed();
        }
        rend.sprite = ButtonOffSprite;
        AudioSource ass = gameObject.PlaySound(ResourceManager.Buttom, GameObjectExtensions.SoundOverrideType.SimultaneousPlaying);
        ass.volume = 0.3f;
        this.Destroy();
    }
}
