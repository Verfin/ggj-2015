﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour 
{
    [HideInInspector]
    public int ButtonHookCount = 0;
    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void ButtonPushed()
    {
        ButtonHookCount--;
        if (ButtonHookCount <= 0)
        {
            Open();
        }
    }

    public void Open()
    {
        gameObject.PlaySound(ResourceManager.Ovi, GameObjectExtensions.SoundOverrideType.SimultaneousPlaying);
        collider2D.enabled = false;
        anim.SetTrigger("Open");
    }
}
