﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cannonball : MonoBehaviour 
{
    public float destroyTimer;
    public bool goBoom;
    private bool goneBoom = false;
    private bool addToList = false;
    private GameObject explosionAnimator;
    private GameObject go;
    private bool explosionAnimated;
	// Use this for initialization
	void Start () 
    {
        explosionAnimator = FindObjectOfType<Cannon>().ExplosionAnimator;
	}

    void Update()
    {
        destroyTimer -= Time.deltaTime;
        if (destroyTimer < 0 && goneBoom == false)
        {
            CannonballControl.SetCannonball(this.gameObject);
            if (GetComponentInChildren<BombDeathRadius>() != null)
            {
                GetComponentInChildren<BombDeathRadius>().destroyTheseGameObjects.TrimExcess();
                foreach (GameObject go in GetComponentInChildren<BombDeathRadius>().destroyTheseGameObjects)
                {
                    go.Destroy();
                }
                GetComponent<ExplosionForce2D>().Explode = true;
                foreach (ExplosionForce2D e in FindObjectsOfType<ExplosionForce2D>())
                {
                    e.Explode = true;
                }
                goneBoom = true;
                gameObject.collider2D.enabled = false;
                GetComponent<SpriteRenderer>().enabled = false;
                if (GetComponentInChildren<Light>() != null)
                {
                    GetComponentInChildren<Light>().enabled = false;
                }
            }

        }
    }
	// Update is called once per frame
	void LateUpdate () 
    {
        if (destroyTimer < -0.1f && !explosionAnimated)
        {
            if (explosionAnimator != null)
            {
                go = (GameObject)GameObject.Instantiate(explosionAnimator, gameObject.transform.position, Quaternion.Euler(0, 0, 0));
            }
            explosionAnimated = true;
        }

        if (destroyTimer < -0.4f)
        {
            go.Destroy();
            this.gameObject.Destroy();
        }
	}


}
