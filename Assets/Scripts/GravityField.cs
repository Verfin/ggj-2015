﻿using UnityEngine;
using System.Collections.Generic;

public class GravityField : MonoBehaviour 
{
    public Vector2 Gravity;
    private List<Rigidbody2D> rigidbodylist;

    public void Awake()
    {
        rigidbodylist = new List<Rigidbody2D>();
    }

    public void Disable()
    {
        foreach (Rigidbody2D rig in rigidbodylist)
        {
            rig.gravityScale = 1f;
        }

        rigidbodylist.Clear();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Rigidbody2D rig = col.rigidbody2D;
        if (rig != null && col.GetComponent<DrainableMatter>() == null)
        {
            rig.gravityScale = 0;
            rigidbodylist.Add(rig);
        }
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.rigidbody2D != null && col.GetComponent<DrainableMatter>() == null)
        {
            col.rigidbody2D.velocity += Gravity*Time.fixedDeltaTime;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        Rigidbody2D rig = col.rigidbody2D;
        if (rig != null && col.GetComponent<DrainableMatter>() == null)
        {
            rig.gravityScale = 1;
            rigidbodylist.Remove(rig);
        }
    }
}
