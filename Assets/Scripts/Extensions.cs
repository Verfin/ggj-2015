﻿    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public static class GameObjectExtensions
    {
        public enum SoundOverrideType
        {
            /// <summary>
            /// Least expensive in processing time. Doesn't stop other audiosources with the same clip from playing
            /// </summary>
            SimultaneousPlaying,
            /// <summary>
            /// If there is any audiosource playing at the same time, doesn't do anything
            /// </summary>
            NoOverriding,
            /// <summary>
            /// Stops other audiosources with the same clip from playing and plays the clip
            /// </summary>
            Override
        }

        public static void Destroy(this UnityEngine.Object gameObject)
        {
            UnityEngine.Object.Destroy(gameObject);
        }

        /// <summary>
        /// Plays an audioclip without the hassle of playing with audiosources
        /// </summary>
        /// <param name="audioClip">Clip to play</param>
        /// <param name="overrideType">How to override already playing clip</param>
        /// <returns>Returns the audio source that is now playing the clip</returns>
        public static AudioSource PlaySound(this GameObject gameObject, AudioClip audioClip, SoundOverrideType overrideType)
        {
            if (audioClip != null)
            {
                if (overrideType == SoundOverrideType.NoOverriding)
                {
                    foreach (AudioSource audioSourc in gameObject.GetComponents<AudioSource>())
                    {
                        if (audioSourc.clip == audioClip)
                        {
                            if (!audioSourc.isPlaying)
                            {
                                audioSourc.Play();
                            }

                            return audioSourc;
                        }
                    }
                }
                else if (overrideType == SoundOverrideType.Override)
                {
                    foreach (AudioSource audioSourc in gameObject.GetComponents<AudioSource>())
                    {
                        if (audioSourc.clip == audioClip)
                        {
                            audioSourc.Stop();
                            audioSourc.Play();
                            return audioSourc;
                        }
                    }
                }

                foreach (AudioSource audioSourc in gameObject.GetComponents<AudioSource>())
                {
                    if (!audioSourc.isPlaying)
                    {
                        audioSourc.clip = audioClip;
                        audioSourc.Play();
                        return audioSourc;
                    }
                }

                AudioSource addedSource = gameObject.AddComponent<AudioSource>();
                addedSource.clip = audioClip;
                addedSource.Play();
                return addedSource;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Plays an audioclip without the hassle of playing around with audiosources
        /// </summary>
        /// <param name="audioClips">Clips to play at random</param>
        /// <param name="overrideType">How to override already playing clip</param>
        /// <returns>Returns the audiosource that is now playing the clip</returns>
        public static AudioSource PlaySound(this GameObject gameObject, AudioClip[] audioClips, SoundOverrideType overrideType)
        {   
            if (audioClips != null)
            {
                if (audioClips.Length > 0)
                {
                    if (overrideType == SoundOverrideType.NoOverriding)
                    {
                        foreach (AudioSource audioSourc in gameObject.GetComponents<AudioSource>())
                        {
                            foreach (AudioClip audioClip in audioClips)
                            {
                                if (audioSourc.clip == audioClip)
                                {
                                    if (!audioSourc.isPlaying)
                                    {
                                        audioSourc.Play();
                                    }

                                    return audioSourc;
                                }
                            }
                        }
                    }
                    else if (overrideType == SoundOverrideType.Override)
                    {
                        foreach (AudioSource audioSourc in gameObject.GetComponents<AudioSource>())
                        {
                            foreach (AudioClip audioClip in audioClips)
                            {
                                if (audioSourc.clip == audioClip)
                                {
                                    audioSourc.Stop();
                                    audioSourc.clip = gameObject.RandomAudio(audioClips);
                                    audioSourc.Play();
                                    return audioSourc;
                                }
                            }
                        }
                    }

                    foreach (AudioSource audioSourc in gameObject.GetComponents<AudioSource>())
                    {
                        if (!audioSourc.isPlaying)
                        {
                            audioSourc.clip = gameObject.RandomAudio(audioClips);
                            audioSourc.Play();
                            return audioSourc;
                        }
                    }

                    AudioSource addedSourc = gameObject.AddComponent<AudioSource>();
                    addedSourc.clip = gameObject.RandomAudio(audioClips);
                    addedSourc.Play();
                    return addedSourc;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Finds a clip and stops it
        /// </summary>
        /// <param name="soundToStop">Clip to stop</param>
        /// <returns>Returns the audio source that was stopped</returns>
        public static void StopSound(this GameObject gameObject, AudioClip soundToStop)
        {
            foreach (AudioSource audioSourc in gameObject.GetComponents<AudioSource>())
            {
                if (audioSourc.clip == soundToStop)
                {
                    audioSourc.Stop();
                }
            }
        }

        /// <summary>
        /// Finds all the clips and stops them if they match with the ones in given array
        /// </summary>
        /// <param name="soundsToStop">Clips to find and stop</param>
        public static void StopSound(this GameObject gameObject, AudioClip[] soundsToStop)
        {
            foreach (AudioSource audioSourc in gameObject.GetComponents<AudioSource>())
            {
                foreach (AudioClip audioClip in soundsToStop)
                {
                    if (audioSourc.clip == audioClip)
                    {
                        audioSourc.Stop();
                    }
                }
            }
        }

        /// <summary>
        /// Pauses all audio sources
        /// </summary>
        public static void PauseAllSounds(this GameObject gameObject)
        {
            foreach (AudioSource audioSourc in gameObject.GetComponents<AudioSource>())
            {
                if (audioSourc.isPlaying)
                {
                    audioSourc.Pause();
                }
                else
                {
                    Component.Destroy(audioSourc);
                }
            }
        }

        /// <summary>
        /// Stops all audiosources
        /// </summary>
        public static void StopAllSounds(this GameObject gameObject)
        {
            foreach (AudioSource audioSourc in gameObject.GetComponents<AudioSource>())
            {
                if (audioSourc.isPlaying)
                {
                    audioSourc.Stop();
                }
                else
                {
                    Component.Destroy(audioSourc);
                }
            }
        }

        /// <summary>
        /// Starts playing all the audiosources
        /// </summary>
        public static void PlayAllSounds(this GameObject gameObject)
        {
            foreach (AudioSource audioSourc in gameObject.GetComponents<AudioSource>())
            {
                if (!audioSourc.isPlaying)
                {
                    audioSourc.Play();
                }
            }
        }

        public static AudioClip RandomAudio(this GameObject gameObject, AudioClip[] audioArray)
        {
            int breaker = 20;
            AudioClip temp = null;
            if (audioArray.Length > 0)
            {
                while (temp == null)
                {
                    temp = audioArray[UnityEngine.Random.Range(0, audioArray.Length)];
                    breaker--;
                    if (breaker <= 0)
                    {
                        Debug.Log("Broke from whileloop. Did you forget to add sounds to " + gameObject.name + "?");
                        break;
                    }
                }
            }

            return temp;
        }
    }