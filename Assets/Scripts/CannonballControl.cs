﻿using UnityEngine;
using System.Collections;

public static class CannonballControl
{
    private static GameObject CannonBall;

    public static void SetCannonball(GameObject cb)
    {
        CannonBall = cb;
    }
    public static GameObject GetCannonball()
    {
        return CannonBall;
    }
}
