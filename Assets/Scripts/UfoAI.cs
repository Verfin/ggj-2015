﻿using UnityEngine;
using System.Collections;

public class UfoAI : MonoBehaviour {
    public float MaximumSpeed;
    public int FacingRight = 1;
    public RaycastHit2D Ray;
    public float RayOffset;
    public float RayLength;
    private Player player;
    private float timer;
    private PortableBlackHoleController blackhole;
	// Use this for initialization
	void Start () {
        player = FindObjectOfType<Player>();
        blackhole = FindObjectOfType<PortableBlackHoleController>();
	}
	
	// Update is called once per frame
    void FixedUpdate()
    {
        timer += Time.fixedDeltaTime/4;
        //rigidbody2D.velocity = new Vector2(transform.right.x * MaximumSpeed * FacingRight, rigidbody2D.velocity.y + Mathf.Sin(timer*60));
        rigidbody2D.velocity = rigidbody2D.velocity + new Vector2(transform.up.x, transform.up.y) * Mathf.Sin(timer*60);

        if (Mathf.Abs(rigidbody2D.velocity.x) < MaximumSpeed)
        {
            rigidbody2D.AddForce(transform.right * MaximumSpeed * FacingRight);
        }
        Ray = Physics2D.Raycast(transform.position + transform.right * RayOffset * FacingRight, transform.right, RayLength);
        if (Ray.collider != null && player != null)
        {
            if (Ray.collider != player.gameObject.collider2D && Ray.collider != blackhole.collider2D.gameObject)
            {
                Flip();
            }
        }


    }

    void Flip()
    {
        FacingRight = -FacingRight;
        RayLength = -RayLength;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position + transform.right * RayOffset * FacingRight, transform.right * RayLength);
    }
}
