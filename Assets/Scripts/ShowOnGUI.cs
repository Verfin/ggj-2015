﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowOnGUI : MonoBehaviour 
{
    public float WaitTime = 3f;
    float timer;
    float waitTimer;
    bool fadingIn;
    Graphic graphic;
    Color startColor;
    Color alphaColor;

    void Awake()
    {
        waitTimer = WaitTime;
        timer = 0;
        fadingIn = false;
        graphic = GetComponent<Graphic>();
        startColor = new Color(graphic.color.r, graphic.color.g, graphic.color.b, 0);
        alphaColor = new Color(startColor.r, startColor.g, startColor.b, 1);
        graphic.color = startColor;
    }

    void Update()
    {
        if (!fadingIn)
        {
            waitTimer -= Time.deltaTime;
            if (waitTimer <= 0)
            {
                fadingIn = true;
            }
        }
        else
        {
            timer += Time.deltaTime;
            if (timer >= 2f)
            {
                timer = 1f;
                this.Destroy();
            }
            graphic.color = Color.Lerp(startColor, alphaColor, timer);
        }
    }
}
