﻿using UnityEngine;
using System.Collections.Generic;

public class PortableBlackHoleController : MonoBehaviour 
{
    public GameObject Box;
    public GameObject Beam;
    public GameObject Sphere;
    public KeyCode fire = KeyCode.Space;
    public KeyCode CreateBox = KeyCode.U;
    public KeyCode CreateBeam = KeyCode.I;
    public KeyCode CreateSphere = KeyCode.O;
    public DrainableLight Light;
    public DrainableMatter Matter;
    public float Energy { get; private set; }
    float maxEnergy = 100f;
    float suckyEnergy = 50f;
    SpriteRenderer rend;
    private Player player;

    void OnTriggerEnter2D(Collider2D col)
    {
        Light = col.GetComponent<DrainableLight>();
        Matter = col.GetComponent<DrainableMatter>();
        player = FindObjectOfType<Player>();
    }


    void OnTriggerExit2D(Collider2D col)
    {
        if (col.GetComponent<DrainableLight>() != null)
        {
            Light = null;   
        }

        if (col.GetComponent<DrainableMatter>() != null)
        {
            Matter = null;   
        }
    }

    void Update () 
    {
        if (Input.GetKey(fire))
        {
            if (Light != null)
            {
                if (Energy >= maxEnergy)
                {
                    Energy = maxEnergy;
                }
                else
                {
                    Light.Amount -= Time.deltaTime * 20f;
                    Energy += Time.deltaTime * 20f;
                    if (Light.Amount <= 0)
                    {
                        if (Light.ChangeSpriteToThisAfterDraining != null)
                        {
                            Light.Destroy();
                            ((SpriteRenderer)Light.renderer).sprite = Light.ChangeSpriteToThisAfterDraining;
                        }
                        else
                        {
                            GameObject.Destroy(Light.gameObject);
                        }
                        Energy = (float)System.Math.Round(Energy, System.MidpointRounding.AwayFromZero);
                    }
                }
            }
            else if (Matter != null)
            {
                if (Energy >= maxEnergy)
                {
                    Energy = maxEnergy;
                }
                else
                {
                    Matter.Amount -= Time.deltaTime * 20f;
                    Energy += Time.deltaTime * 20f;
                    if (Matter.Amount <= 0)
                    {
                        GameObject.Destroy(Matter.gameObject);
                    }
                    else if (Matter.Amount < Matter.StartAmount * 0.20f)
                    {
                        Energy += Matter.Amount;
                        Energy = (float)System.Math.Round(Energy, System.MidpointRounding.AwayFromZero);
                        GameObject.Destroy(Matter.gameObject);
                    }
                }
            }

            rend.color = Color.Lerp(Color.white, new Color(1f, 0.4f, 0f), Energy / 100f);
        }
        else if (Input.GetKeyDown(CreateBeam))
        {
            if (Energy >= suckyEnergy)
            {
                GameObject.Instantiate(Beam, transform.position + transform.right * player.transform.localScale.x, transform.rotation);
                Energy -= suckyEnergy;
                rend.color = Color.Lerp(Color.white, new Color(1f, 0.4f, 0f), Energy / 100f);
            }
        }
        else if (Input.GetKeyDown(CreateBox))
        {
            if (Energy >= suckyEnergy)
            {
                GameObject.Instantiate(Box, transform.position + transform.right * player.transform.localScale.x, transform.rotation);
                Energy -= suckyEnergy;
                rend.color = Color.Lerp(Color.white, new Color(1f, 0.4f, 0f), Energy / 100f);
            }
        }
        else if (Input.GetKeyDown(CreateSphere))
        {
            if (Energy >= suckyEnergy)
            {
                GameObject.Instantiate(Sphere, transform.position + transform.right * player.transform.localScale.x, transform.rotation);
                Energy -= suckyEnergy;
                rend.color = Color.Lerp(Color.white, new Color(1f, 0.6f, 0f), Energy / 100f);
            }
        }
	}

    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
    }
}
