﻿using UnityEngine;
using System.Collections;

public class BlackBoxHolder : MonoBehaviour {
    private Quaternion Startvalues;
	// Use this for initialization
	void Start () {
        Startvalues = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
        transform.rotation = Startvalues;
	}
    void LateUpdate()
    {
        transform.rotation = Startvalues;
    }
}
