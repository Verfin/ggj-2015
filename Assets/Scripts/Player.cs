﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    public bool FacingRight;
    public float MoveSpeed;
    public float MaxSpeed = 5;
    public float Acceleration = 30;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.localScale.x > 0)
        {
            FacingRight = true;
        }
        else
        {
            FacingRight = false;
        }
	}

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.D))
        {
            if (!FacingRight)
            {
                Flip();
            }

            if (rigidbody2D.velocity.x < MaxSpeed)
            {
                rigidbody2D.AddForce(Vector2.right * Acceleration);
            }
        }
        else if (Input.GetKey(KeyCode.A))
        {
            if (FacingRight)
            {
                Flip();
            }

            if (rigidbody2D.velocity.x > -MaxSpeed)
            {
                rigidbody2D.AddForce(-Vector2.right * Acceleration);
            }
        }
    }

    public void Flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
}
