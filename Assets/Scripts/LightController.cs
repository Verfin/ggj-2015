﻿using UnityEngine;
using System.Collections;

public class LightController : MonoBehaviour
{
    public GameObject LightGameObject;
    private Vector3 LightAngle;
    private float AngleLight = 1;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.localScale.x < 0 && (LightGameObject.transform.rotation.eulerAngles.z > 270 || LightGameObject.transform.rotation.eulerAngles.z < 90))
        {
            LightGameObject.transform.rotation = Quaternion.Euler(new Vector3(LightGameObject.transform.rotation.eulerAngles.x, LightGameObject.transform.rotation.eulerAngles.y, 180 - LightGameObject.transform.rotation.eulerAngles.z));
        }
        else if (gameObject.transform.localScale.x > 0 && (LightGameObject.transform.rotation.eulerAngles.z > 90 && LightGameObject.transform.rotation.eulerAngles.z < 270))
        {
            LightGameObject.transform.rotation = Quaternion.Euler(new Vector3(LightGameObject.transform.rotation.eulerAngles.x, LightGameObject.transform.rotation.eulerAngles.y, 180 - LightGameObject.transform.rotation.eulerAngles.z));
        }

        if (gameObject.transform.localScale.x > 0)
        {
            if (Input.GetKey(KeyCode.DownArrow) && (LightGameObject.transform.rotation.eulerAngles.z > 272 || LightGameObject.transform.rotation.eulerAngles.z < 95))
            {
                LightGameObject.transform.Rotate(new Vector3(0, 0, 1), -AngleLight);
            }
            else if (Input.GetKey(KeyCode.UpArrow) && (LightGameObject.transform.rotation.eulerAngles.z < 88 || LightGameObject.transform.eulerAngles.z > 265))
            {
                LightGameObject.transform.Rotate(new Vector3(0, 0, 1), AngleLight);
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.UpArrow) && (LightGameObject.transform.rotation.eulerAngles.z < 275 && LightGameObject.transform.rotation.eulerAngles.z > 92))
            {
                LightGameObject.transform.Rotate(new Vector3(0, 0, 1), -AngleLight);
            }
            else if (Input.GetKey(KeyCode.DownArrow) && (LightGameObject.transform.rotation.eulerAngles.z > 88 && LightGameObject.transform.eulerAngles.z < 268))
            {
                LightGameObject.transform.Rotate(new Vector3(0, 0, 1), AngleLight);
            }
        }

    }
}