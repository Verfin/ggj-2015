﻿using UnityEngine;
using System.Collections;

public class BasicBullet : MonoBehaviour 
{
    public float destroyTimer;
    public bool DestroyedInTime;
    public bool Indestructible = false;

    void OnTriggerEnter2D(Collider2D col)
    {
        Player player = col.GetComponent<Player>();
        PortableBlackHoleController blackhole = col.GetComponent<PortableBlackHoleController>();
        if (player != null || blackhole != null)
        {
            FindObjectOfType<Player>().GetComponent<DestroyOnCollision>().Death();
            //player.GetComponent<DestroyOnCollision>().Death();
        }

        if (DestroyedInTime && !Indestructible)
        {
            gameObject.Destroy();
        }
    }

    void Update()
    {
        if (DestroyedInTime)
        {
            destroyTimer -= Time.deltaTime;
            if (destroyTimer < 0)
            {
                gameObject.Destroy();
            }
        }
    }
}
