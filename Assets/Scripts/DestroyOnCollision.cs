﻿using UnityEngine;
using System.Collections;

public class DestroyOnCollision : MonoBehaviour 
{
    [HideInInspector]
    public float tempVeloX;
    [HideInInspector]
    public float tempVeloY;
    private float saveVeloTimer;
    private Vector2 tempPos;
    private bool areWeDeadYet;
    private float dyingTimer;
    private float angularSpeed;
	// Use this for initialization
	void Start () 
    {
        dyingTimer = 3;
        angularSpeed = 10;
        saveVeloTimer = 0.15f;
        tempPos = gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
    {
        saveVeloTimer -= Time.deltaTime;

        if (saveVeloTimer < 0)
        {
            tempVeloX = gameObject.rigidbody2D.velocity.x;
            tempVeloY = gameObject.rigidbody2D.velocity.y;
            saveVeloTimer = 0.15f;
        }

        tempVeloX = Mathf.Abs(gameObject.rigidbody2D.velocity.x);// - tempVeloX);
        tempVeloY = Mathf.Abs(gameObject.rigidbody2D.velocity.y);//- tempVeloX);
	}

    void LateUpdate()
    {
        if (areWeDeadYet)
        {
            dyingTimer -= Time.deltaTime;

            if (dyingTimer < 0)
            {
                if (GetComponent<Player>() != null)
                {
                    Application.LoadLevel(Application.loadedLevel);
                }
                else
                {
                    gameObject.Destroy();
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if ((tempVeloX > 25) || tempVeloY > 25)
        {
            Death();
        }
        if (col.rigidbody != null && col.gameObject.GetComponent<DestroyOnCollision>() != null)
        {
            //if (Mathf.Abs(col.rigidbody.velocity.x) > 9 || Mathf.Abs(col.rigidbody.velocity.y) > 9)
            if(col.gameObject.GetComponent<DestroyOnCollision>().tempVeloX > 30 || col.gameObject.GetComponent<DestroyOnCollision>().tempVeloY > 30)
            {
                Death();
            }
        }
    }

    public void Death()
    {
        foreach (MonoBehaviour mb in this.gameObject.GetComponents<MonoBehaviour>())
        {
            if (mb != this)
            {
                mb.enabled = false;
            }
        }

        gameObject.rigidbody2D.fixedAngle = false;
        areWeDeadYet = true;
        gameObject.rigidbody2D.AddTorque(100);
        gameObject.GetComponent<SpriteRenderer>().color = new Color(16581380, 16581380, 16581380, 0.75f);

        if (gameObject.GetComponent<Player>() != null)
        {
            LoadNextLevelOnPlayerTouch lnlopt = FindObjectOfType<LoadNextLevelOnPlayerTouch>();
            if (lnlopt != null)
            {
                lnlopt.collider2D.enabled = false;
            }
            FindObjectOfType<DragRigidBody2D>().enabled = false;
        }
    }
}
