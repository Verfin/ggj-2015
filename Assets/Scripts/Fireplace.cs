﻿using UnityEngine;
using System.Collections;

public class Fireplace : MonoBehaviour 
{
    void Start()
    {
        AudioSource ass = gameObject.PlaySound(ResourceManager.Burning, GameObjectExtensions.SoundOverrideType.SimultaneousPlaying);
        ass.loop = true;
        ass.volume = 0.3f;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        WoodenBranch wb = col.GetComponent<WoodenBranch>();
        Player player = col.GetComponent<Player>();
        if (player != null)
        {
            player.GetComponent<DestroyOnCollision>().Death();
        }
        else if (wb != null)
        {
            wb.Fire();
        }
    }
}
