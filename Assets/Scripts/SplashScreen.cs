﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SplashScreen : MonoBehaviour 
{
    private enum State
    { 
        FadeIn,
        Show,
        FadeOut
    }

    public float FadeInTime = 0.5f;
    public float ShowTime = 2;
    public float FadeOutTime = 0.5f;
    public bool LoadLevelAfter = true;
    private float fadeInTime;
    private float showTime;
    private float fadeOutTime;
    Image image;
    State state;

	// Use this for initialization
	void Start () 
    {
        state = State.FadeIn;
        image = GetComponent<Image>();
        fadeInTime = FadeInTime;
        showTime = ShowTime;
        fadeOutTime = FadeOutTime;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (state == State.FadeIn)
        {
            fadeInTime -= Time.deltaTime;
            if (fadeInTime <= 0)
            {
                image.color = new Color(1, 1, 1, 1);
                state = State.Show;
            }
            else
            {
                image.color = new Color(1, 1, 1, 1 - fadeInTime / FadeInTime);
            }
        }
        else if (state == State.Show)
        {
            showTime -= Time.deltaTime;
            if (showTime <= 0)
            {
                state = State.FadeOut;
            }
        }
        else
        {
            fadeOutTime -= Time.deltaTime;
            if (fadeOutTime <= 0)
            {
                image.color = new Color(1, 1, 1, 0);
                if (LoadLevelAfter)
                    Application.LoadLevel(1);
            }
            else
            {
                image.color = new Color(1, 1, 1, fadeOutTime / FadeInTime);
            }
        }
	}
}