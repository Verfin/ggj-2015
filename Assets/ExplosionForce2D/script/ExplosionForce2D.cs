﻿// Developed by Ananda Gupta
// info@anandagupta.com
// http://anandagupta.com

using UnityEngine;
using System.Collections;

public class ExplosionForce2D : MonoBehaviour
{
	public float Power;
	public float Radius;
    public bool Explode;
    private Cannonball cb;
    public bool explodable;
	
		// Use this for initialization
		void Start ()
		{
            cb = GetComponent<Cannonball>();
		}
	
		// Update is called once per frame
		void FixedUpdate ()
		{
			if (Explode == true && explodable)// || Input.GetButtonDown("Fire1") && explodable)
            {
				//Vector3 mousePos = Input.mousePosition;
				//mousePos.z = 10;
				//Vector3 objPos1 = Camera.main.ScreenToWorldPoint(mousePos);
                Vector3 objPos1 = new Vector3();
                

                /*foreach (Cannonball canb in FindObjectsOfType<Cannonball>())
                {
                    objPos1 = canb.transform.position;
                }*/
                objPos1 = CannonballControl.GetCannonball().transform.position;
				AddExplosionForce(rigidbody2D, Power * 100, objPos1, Radius);
                //objPos1 = gameObject.transform.position + new Vector3(-2, 1, 0);
                //AddExplosionForce(rigidbody2D, Power * 100, objPos1, Radius);
                if (cb != null)
                {
                    cb.goBoom = false;
                }

                Explode = false;
			}
		}

		public static void AddExplosionForce (Rigidbody2D body, float expForce, Vector3 expPosition, float expRadius)
		{
				var dir = (body.transform.position - expPosition);
				float calc = 1 - (dir.magnitude / expRadius);
				if (calc <= 0) {
						calc = 0;		
				}

				body.AddForce (dir.normalized * expForce * calc);
		}


}
